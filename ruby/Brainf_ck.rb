# brainf*ck

if File.basename($PROGRAM_NAME) == File.basename(__FILE__)
    TableFile = ARGV.size > 0 ? ARGV[0] : "table.txt"
    CodeFile = ARGV.size > 1 ? ARGV[1] : "code.txt"
    
    # BrainF_ck.run CodeFile, TableFile
    
    table = BrainF_ck.makeCmdTable TableFile
    code = BrainF_ck.compile table, CodeFile
    BrainF_ck.useDefaultTable true
    puts code.to_source
end

BEGIN {

class BrainF_ck
    
    @@MEM_SIZE = 20000
    @@cmdList = [:INC_PTR, :DEC_PTR, :INC_VAL, :DEC_VAL, :WHILE, :WEND, :INPUT, :OUTPUT]
    
    class BrainF_ck::CmdTable
        protected def initialize(word2cmd, headAndMaxlen)
            @word2cmd = word2cmd.dup
            @headAndMaxlen = headAndMaxlen.dup
        end
        def get
            [@word2cmd.dup, @headAndMaxlen.dup]
        end
    end
    
    class BrainF_ck::CodeContainer
        protected def initialize(code)
            @code = code.dup
        end
        def get
            @code.dup
        end
    end
    
    class BrainF_ck::CompiledCode < BrainF_ck::CodeContainer
        def to_source(cmdTable = "")
            word2cmd, _ = (cmdTable.instance_of?(BrainF_ck::CmdTable) ? cmdTable : BrainF_ck::makeCmdTable(cmdTable)).get
            cmd = {}
            word2cmd.each do |w,c|
                cmd[c] = w
            end
            @code.map{|c| cmd[c]} * ""
        end
    end
    
    class BrainF_ck::CompactCode < BrainF_ck::CodeContainer
    end
    
    class BrainF_ck::Error < RuntimeError
    end
    
    @@useDefaultTable = false
    
    def BrainF_ck.useDefaultTable(use)
        @@useDefaultTable = true & use
    end
    
    def BrainF_ck.useDefaultTable?
        @@useDefaultTable
    end
    
    def BrainF_ck.makeCmdTable(file = "", sep = ":")
        
        lines =
            if File.exist? file
                begin
                    f = open(file)
                    f.readlines
                ensure
                    f.close if !f.nil?
                end
            elsif BrainF_ck.useDefaultTable?
                "><+-[],.".chars
            else
                raise BrainF_ck::Error, "%s is not found" % [file.to_s]
            end
        
        raise BrainF_ck::Error, "need more command lines in table" if lines.size < 8
        
        sp = proc { |c|
            r = lines.shift.chomp.split(sep) - [""]
            raise BrainF_ck::Error, "command is not found in table: %s" % [c.to_s] if r.empty?
            r
        }
        
        word2cmd = {}
        headAndMaxlen = Hash.new 0
        
        @@cmdList.each do |c|
            sp.call(c).each do |w|
                word2cmd[w] = c
                h = w[0]
                headAndMaxlen[h] = [headAndMaxlen[h], w.size].max
            end
        end
        
        BrainF_ck::CmdTable.new word2cmd, headAndMaxlen
    end
    
    def BrainF_ck.compile(cmdTable = "", sourcefile)
        raise BrainF_ck::Error, "%s is not found" % [sourcefile.to_s] if !File.exist? sourcefile
        f = open(sourcefile)
        source = f.read
        
        word2cmd, headAndMaxlen =
            (cmdTable.instance_of?(BrainF_ck::CmdTable) ? cmdTable : BrainF_ck.makeCmdTable(cmdTable)).get
        
        wc = 0
        pos = 0
        code = []
        w = ""
        source.each_char do |ch|
            pb = [].push ch
            while !pb.empty?
                w << pb.pop
                if word2cmd.include? w
                    cmd = word2cmd[w]
                    case cmd
                    when :WHILE
                        wc += 1
                    when :WEND
                        raise BrainF_ck::Error, "bad position of wend in source: %d" % [pos] if wc == 0
                        wc -= 1
                    end
                    code << cmd
                    pos += w.size
                elsif headAndMaxlen.include? w[0]
                    if headAndMaxlen[w[0]] <= w.size
                        pb += w[1..-1].reverse.chars
                        pos += 1
                    else
                        next
                    end
                else
                    pos += 1
                end
                w = ""
            end
        end
        raise BrainF_ck::Error, "need %d wends in source" % [wc] if wc > 0
        
        BrainF_ck::CompiledCode.new code
    ensure
        f.close if !f.nil?
    end
    
    protected def makeJumpTable(code)
        jumpTable = {}
        stack = []
        code.size.times do |i|
            case code[i][0]
            when :WHILE
                stack.push i
            when :WEND
                raise BrainF_ck::Error, "bad position of wend in code: %d" % [i] if stack.empty?
                iWhile = stack.pop
                iWend = i
                jumpTable[iWhile] = iWend
                jumpTable[iWend] = iWhile - 1
            end
        end
        
        jumpTable
    end
    
    def BrainF_ck.compact(codeData, cmdTable = "")
        code = (codeData.instance_of?(BrainF_ck::CompiledCode) ? codeData : BrainF_ck.compile(cmdTable, codeData)).get
        
        ccode = []
        bcmd = nil
        value = 0
        code.each do |cmd|
            if cmd != bcmd
                ccode << [bcmd, value] if !bcmd.nil?
                bcmd = cmd
                value = 1
                next
            end
            case cmd
            when :INC_PTR, :INC_VAL, :DEC_PTR, :DEC_VAL, :INPUT, :OUTPUT
                value += 1
            when :WHILE, :WEND
                ccode << [bcmd, 0]
            else
                raise BrainF_ck::Error, "unknown code: %s" % [cmd.to_s]
            end
        end
        ccode << [bcmd, value] if !bcmd.nil?
        
        CompactCode.new ccode
    end
    
    def initialize(cmdTable = "", input = $stdin, output = $stdout)
        raise BrainF_ck::Error, "input is not IO in initialize" if !input.instance_of?(IO)
        raise BrainF_ck::Error, "output is not IO in initialize" if !output.instance_of?(IO)
        @mem = [0] * @@MEM_SIZE
        @ptr = 0
        @cmdTable = cmdTable.instance_of?(BrainF_ck::CmdTable) ? cmdTable : BrainF_ck.makeCmdTable(cmdTable)
        @input = input
        @output = output
    end
    
    def clear()
        @mem.fill 0
        @ptr = 0
        self
    end
    
    def setInput(input)
        raise BrainF_ck::Error, "input is not IO in setInput" if !input.instance_of?(IO)
        @input = input
        self
    end
    
    def setOutput(output)
        raise BrainF_ck::Error, "output is not IO in setOutput" if !output.instance_of?(IO)
        @output = output
        self
    end
    
    def setCmdTable(cmdTable = "")
        @cmdTable = cmdTable.instance_of?(BrainF_ck::CmdTable) ? cmdTable : BrainF_ck.makeCmdTable(cmdTable)
        self
    end
    
    def run(codeData, cmdTable = @cmdTable)
        code = (codeData.instance_of?(BrainF_ck::CompactCode) ? codeData : BrainF_ck.compact(codeData, cmdTable)).get
        
        jumpTable = makeJumpTable code
        
        i = 0
        
        while i < code.size
            cmd, value = code[i]
            case cmd
            when :INC_PTR
                @ptr = [@ptr + value, @mem.size.pred].min
            when :DEC_PTR
                @ptr = [@ptr - value, 0].max
            when :INC_VAL
                @mem[@ptr] = @mem[@ptr].+(value) & 255
            when :DEC_VAL
                @mem[@ptr] = @mem[@ptr].-(value) & 255
            when :WHILE
                i = jumpTable[i] if @mem[@ptr] == 0
            when :WEND
                i = jumpTable[i]
            when :INPUT
                value.times do
                    @mem[@ptr] = @input.readbyte
                end
            when :OUTPUT
                value.times do
                    @output.putc @mem[@ptr]
                end
            else
                raise BrainF_ck::Error, "unknown cmd: %d ... %s" % [i, cmd[0].to_s]
            end
            i += 1
        end
        self
    end
    
    def BrainF_ck.run(codeData, cmdTable = "", input = $stdin, output = $stdout)
        BrainF_ck.new(cmdTable, input, output).run codeData
    end
    
end
}
